const assert = require('assert');
var Car = require('./cars.js');
var Ferry = require('./ferry.js');
var Board = new Ferry(40, 10);

describe('Car', function() {
  it("should return car color(black) and passenger(20)'", function() {
    var car = new Car ("black", 20);
    assert.equal(car.color, "black");
    assert.equal(car.passenger, 20);
  });
});

describe('Ferry', function(){
  it("should return the number of people in the car'", function(){
    assert.equal(Board.maxPeople, 40);
  })
  it("should return the number of cars in the ferry'", function(){
    assert.equal(Board.maxCars, 10);
  })
  it("should return accepted when the ferry does not reach the max'", function(){
    console.log(Board.onBoard(new Car('black', 4, 'CA 12345')));
    console.log(Board.onBoard(new Car('black', 6, 'CA 12345')));
    console.log(Board.onBoard(new Car('black', 8, 'CA 12345')));
    data= Board.onBoard(new Car('black', 4, 'CA 12345'));
    assert.equal(data, "Accepted");
  })
  it("should return rejected when ferry have reached the maximum'", function(){
    console.log(Board.onBoard(new Car('black', 10, 'CA 12345')));
    console.log(Board.onBoard(new Car('black', 5, 'CA 12345')));
    console.log(Board.onBoard(new Car('black', 5, 'CA 12345')));
    console.log(Board.onBoard(new Car('black', 5, 'CA 12345')));
    console.log(Board.onBoard(new Car('black', 1, 'CA 12345')));
    console.log(Board.onBoard(new Car('black', 1, 'CA 12345')));
    console.log(Board.onBoard(new Car('black', 1, 'CA 12345')));
    console.log(Board.onBoard(new Car('black', 6, 'CA 12345')));
    console.log(Board.onBoard(new Car('black', 8, 'CA 12345')));
    console.log(Board.onBoard(new Car('black', 4, 'CA 12345')));
    console.log(Board.onBoard(new Car('black', 6, 'CA 12345')));
    console.log(Board.onBoard(new Car('black', 8, 'CA 12345')));
    data= Board.onBoard(new Car('black', 4, 'CA 12345'));
    assert.equal(data, "Rejected");
  })
})
