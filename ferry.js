var Car = require('./cars.js');
function Ferry(maxPeople, maxCars ){
this.maxPeople=maxPeople;
this.maxCars=maxCars;
this.ferryObjects={};
this.people_count=0;
this.carArray=[];

this.onBoard=function(car){
  if(this.carArray.length+1<=maxCars && this.people_count+car.passenger<=maxPeople){
    this.people_count+=car.passenger;
    this.carArray.push(car);

    if(!this.ferryObjects[car.regNum]){
      this.ferryObjects[car.regNum]=1;
    }
  else{
    this.ferryObjects[car.regNum]++;
  }
  if(this.ferryObjects[car.regNum]===3) return "You will travel in a half price";
  else if(this.ferryObjects[car.regNum]===7) return "You will travel for free";
  else return 'Accepted';
}

else{
  return 'Rejected';
}
}
}

module.exports=Ferry;
var Board = new Ferry(40, 10);
